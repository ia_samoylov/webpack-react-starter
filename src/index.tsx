import * as React from 'react';
import { render } from 'react-dom';
import { App } from './Application';
import { AppContainer } from 'react-hot-loader';
import './style/index.scss';

import { setConfig } from 'react-hot-loader'

setConfig({ logLevel: 'debug' })

render(
  <App />,
  document.getElementById('root'),
)