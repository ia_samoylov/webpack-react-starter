// @ts-nocheck
const path = require('path');
module.exports = () => {
    return {
        module: {
            rules: [
                {
                    test: /\.(ts|tsx)$/,
                    loader: "awesome-typescript-loader",
                    options: {
                        silent: process.argv.indexOf("--json") !== -1,
                        useBabel: true,
                        babelOptions: {
                            plugins: [
                                "@babel/plugin-syntax-typescript",
                                "@babel/plugin-syntax-decorators",
                                "@babel/plugin-syntax-jsx",
                                "react-hot-loader/babel"
                            ]
                        },
                        babelCore: "@babel/core",
                    },
                    exclude: /node_modules\/src/
                },
            ]
        }
    }
};
