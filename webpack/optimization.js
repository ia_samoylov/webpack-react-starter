// @ts-nocheck
module.exports = (mode) => {
    return {
        optimization: {
            minimize: mode === 'production',
            runtimeChunk: {
                name: 'vendor'
            },
            splitChunks: {
                cacheGroups: {
                    default: false,
                    commons: {
                        test: /node_modules/,
                        name: "vendor",
                        chunks: "initial",
                        minSize: 1
                    }
                }
            }
        }
    }
};