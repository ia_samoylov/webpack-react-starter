// @ts-nocheck
const HtmlWebPackPlugin = require('html-webpack-plugin');

module.exports = (mode, paths) => ({
    module: {
        rules: [
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'html-loader',
                        options: { minimize: mode === 'production' }
                    }
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: `${paths.src}/${paths.rootFileName}.html`,
            filename: './index.html'
        })
    ]
});