

// @ts-nocheck

module.exports = (paths) => {
    return {
        entry: `${paths.src}/${paths.rootFileName}.tsx`,
        output: { filename: `./js/[name].[hash].js`, path: `${paths.dist}` },
        resolve: {
            extensions: [".ts", ".tsx", ".js"]
        }
    }
};
