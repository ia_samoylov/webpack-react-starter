// @ts-nocheck

module.exports = (mode) => {
    if (mode === 'production')
        return {};

    return {
        devServer: {
            proxy:
                {
                    "/api": "http://localhost:5001"
                }
        },
        devtool: "cheap-module-eval-source-map",
    }
};
