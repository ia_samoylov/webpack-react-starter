// @ts-nocheck

const WebpackCleanPlugin = require('webpack-clean-plugin');

module.exports = (paths) => ({
    plugins: [
        new WebpackCleanPlugin({
            on: "emit",
            path: [paths.dist]
        })
    ]
});
