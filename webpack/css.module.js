// @ts-nocheck
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');

module.exports = (mode) => {

    return {
        module: {
            rules: [
                {
                    test: /\.scss$/,
                    use: ExtractTextPlugin.extract({
                        use: [
                            { loader: 'css-loader' },
                            {
                                loader: 'postcss-loader',
                                options: {
                                    plugins: () => {
                                        if (mode !== 'production') {
                                            return [];
                                        }

                                        return [
                                            require('precss'),
                                            autoprefixer({
                                                browsers: [
                                                    '>1%',
                                                    'last 4 versions',
                                                    'Firefox ESR',
                                                    'not ie < 9',
                                                ],
                                                flexbox: 'no-2009',
                                            }),
                                            require('cssnano')
                                        ];
                                    }
                                }
                            },
                            { loader: 'sass-loader' }
                        ],
                        fallback: 'style-loader'
                    })
                }
            ],
        },
        plugins: [
            new ExtractTextPlugin({
                filename: "./css/[name].[hash].css"
            })
        ],
    };
};