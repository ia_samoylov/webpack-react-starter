const webpack = require('webpack');
const merge = require('webpack-merge');
const path = require('path');

const paths = {
    rootFileName: 'index',
    src: path.join(__dirname, './src'),
    dist: path.join(__dirname, './wwwroot'),
}

module.exports = (env, argv) => {
    console.log(`Build for ${argv.mode} mode`)
    return merge([
        require('./webpack/clean.plugins')(paths),
        require('./webpack/base')(paths),
        require('./webpack/ts.module')(),
        require('./webpack/html.plugins')(argv.mode, paths),
        require('./webpack/optimization')(argv.mode),
        require('./webpack/css.module')(argv.mode),
        require('./webpack/images.module')(),
        require('./webpack/dev.module')(argv.mode),
    
    ]);
};